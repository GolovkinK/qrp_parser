import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

/**
 * Created by GKI on 26.04.2017.
 */
public class QrpParser {
    private  static ArrayList<OneChannel> resultPacketOfChannels = new ArrayList<>();
    //записываем сюда по очереди все найденные строки и как найдём последнюю (строка A-PID), то скидываем этот канал в массив packetFromOneFile
    private  static OneChannel currentChannel = new OneChannel();
    //это список всех каналов из одного файла, нужен, т.к. дата файла только в конце и её приписываем ко всем записям
    private static ArrayList<OneChannel> packetFromOneFile = new ArrayList<>();

    public static void main(String[] args) {
        try {
            recurseProcessFilesFromFolder(new File(new File("").getAbsolutePath()));
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        //подчищаем результирующий файл от ненужных дубликатов первых строк и записей null
        if (resultPacketOfChannels.size() > 0) {
            ArrayList<OneChannel> temp = new ArrayList<>();
            temp.add(resultPacketOfChannels.get(0));
            for (int i = 0; i < resultPacketOfChannels.size(); i++) {
                if (    "№".equals(resultPacketOfChannels.get(i).getNumb()) &&
                        "Название".equals(resultPacketOfChannels.get(i).getName()) &&
                        "Тип сервиса".equals(resultPacketOfChannels.get(i).getTypOfService()) ) {
                    //пропускаем
                } else {
                    temp.add(resultPacketOfChannels.get(i));
                }
            }
            resultPacketOfChannels = temp;
        }

 //       for (OneChannel ch:resultPacketOfChannels) {
 //           System.out.println(ch);
 //       }
        System.out.println("Запись в CSV-файл...");
        //запишем всё в CSV файл.
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream("Все каналы из QRP файлов.csv"));
            for (int i = 0; i < resultPacketOfChannels.size(); i++) {
                String s = resultPacketOfChannels.get(i).getNumb()+","+
                        resultPacketOfChannels.get(i).getName()+","+
                        resultPacketOfChannels.get(i).getTypOfService()+","+
                        resultPacketOfChannels.get(i).getState()+","+
                        resultPacketOfChannels.get(i).getKodir()+","+
                        resultPacketOfChannels.get(i).getVpid()+","+
                        resultPacketOfChannels.get(i).getVpid()+","+
                        resultPacketOfChannels.get(i).getDatestr()+","+
                        resultPacketOfChannels.get(i).getComment()+"\n";
                bos.write(s.getBytes("windows-1251"));
            }
            bos.close();
            System.out.println("CSV-файл успешно записан!");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    private static void recurseProcessFilesFromFolder(File folder) throws IOException
    {
        File[] folderEntries = folder.listFiles();
        for (File entry : folderEntries)
        {
            if (entry.isDirectory())
            {
                recurseProcessFilesFromFolder(entry);
            }
            // иначе попался файл, обрабатываем его!
            if (entry.getName().endsWith(".QRP") || entry.getName().endsWith(".qrp")) {
                System.out.println(entry.getAbsolutePath());
                parseQrpFile(entry.getAbsolutePath());
            }
        }
    }

    /**
     * Этот метод находит все нужные строки по ключевым байтам в файле, добавляет их в текущий канал currentChannel, потом,
     * когда заполнен текущий канал, кроме даты и комментария, добавляем текущий канал в массив пакета каналов packetFromOneFile,
     * добытых из одного файла, когда доходим до конца файла, добавляем дату и комментарий ко всем записям.
     * Переносим все записи в результирующий список packetOfChannels.
     * После всего обнуляем массив текущего канала packetFromOneFile.
     * @param fileName - имя текущего файла для анализа.
     */
    private static void parseQrpFile(String fileName) {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName), StandardCharsets.UTF_16LE));
            ArrayList<Integer> arr = new ArrayList<>();
            while (reader.ready()) {
                arr.add(reader.read());
            }
            reader.close();
            //      printFile(arr);
            System.out.println("---Начало метода parseQrpFile---");
            for (int i = 0; i < arr.size(); i++) {
                //искать байт начала названия каналов
                String s = searchStrAfterByte(arr,0x00dd,i);
                if (s != null) {
                    currentChannel.setName(s);
                }
                //искать байт начала номера канала
                s = searchStrAfterByte(arr,0x0039,i);
                if (s != null) {
                    currentChannel.setNumb(s);
                }
                //искать байт начала типа сервиса
                s = searchStrAfterByte(arr,0x01a7,i);
                if (s != null) {
                    currentChannel.setTypOfService(s);
                }
                //искать байт начала состояния
                s = searchStrAfterByte(arr,0x022e,i);
                if (s != null) {
                    currentChannel.setState(s);
                }
                //искать байт начала наличия кодирования
                s = searchStrAfterByte(arr,0x02b5,i);
                if (s != null) {
                    currentChannel.setKodir(s);
                }
                //искать байт начала V-PID
                s = searchStrAfterByte(arr,0x033c,i);
                if (s != null) {
                    currentChannel.setVpid(s);
                }
                //искать байт начала A-PID
                s = searchStrAfterByte(arr,0x03c7,i);
                if (s != null) {
                    currentChannel.setApid(s);
                    if (currentChannel.getNumb() == null) {currentChannel.setNumb("");}
                    if (currentChannel.getName() == null) {currentChannel.setName("");}
                    if (currentChannel.getTypOfService() == null) {currentChannel.setTypOfService("");}
                    if (currentChannel.getState() == null) {currentChannel.setState("");}
                    if (currentChannel.getKodir() == null) {currentChannel.setKodir("");}
                    if (currentChannel.getApid() == null) {currentChannel.setApid("");}
                    if (currentChannel.getVpid() == null) {currentChannel.setVpid("");}
                    if (currentChannel.getDatestr() == null) {currentChannel.setDatestr("");}
                    if (currentChannel.getComment() == null) {currentChannel.setComment("");}
                    packetFromOneFile.add(currentChannel);
                    System.out.println();
                    //переходим к следующему каналу
                    currentChannel = new OneChannel();
                }
                //искать байт начала даты
                s = searchStrAfterByte(arr,0x043f,i);
                if (s != null) {
                    currentChannel.setDatestr(s); ;
                    //для всех найденных каналов из текущего файла устанавливаем дату
                    // и также устанавливаем комментарий - имя текущего файла.
                    for (OneChannel ch:packetFromOneFile) {
                        ch.setDatestr(s);
                        //  ch.setComment(fileName.substring(fileName.lastIndexOf(File.separator)+1,fileName.length()));
                        ch.setComment(fileName);
                    }
                    resultPacketOfChannels.addAll(packetFromOneFile);
                    //обнуляем список текущего канала
                    packetFromOneFile = new ArrayList<>();
                }
            }
            System.out.println();

            System.out.println("---Конец метода parseQrpFile---");

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Этот метод возвращает строку из файла от начала через 24 байта после указанного в byteToSeachValue до первого
     * найденного значения байта 0000 или NULL, если ничего не найдено.
     * Перед указанным байтом, конечно, должны идти захардкоженные байты.
     * @param arrayReadedFromFile массив байт, считанный из файла .QRP
     * @param byteToSeachValue байт-идентификатор нужных данных, например 0x00dd для названия канала, 0x03c7 - A-PID,
     * 0x043f - время и дата и т.д.
     * @param
     */
    private static String searchStrAfterByte (ArrayList<Integer> arrayReadedFromFile,int byteToSeachValue, int i) {

        StringBuilder sb = new StringBuilder();
        if (arrayReadedFromFile.get(i)==0xe79e
                && arrayReadedFromFile.get(i+1)==0x41d3
                && arrayReadedFromFile.get(i+2)==0xcf3d
                && arrayReadedFromFile.get(i+3)==0x41d3
                && arrayReadedFromFile.get(i+4)==byteToSeachValue) {
            int j = i+24;
            while (arrayReadedFromFile.get(j) != 0) {
                if (Character.isBmpCodePoint(arrayReadedFromFile.get(j)) && arrayReadedFromFile.get(j)>0xa) {
      //              System.out.print((char)(int)arrayReadedFromFile.get(j));
                    sb.append((char)(int)arrayReadedFromFile.get(j));
      //              System.out.print(Integer.toHexString(arrayReadedFromFile.get(j))+" ");
                }
                j++;
            }
            System.out.print(sb.toString()+" ");
            return sb.toString();
        } else {
            return null;
        }
    }


    /**   вывод на экран массива байт и соответствующих им символов
     * не выводит символы последних 2 строк, а байты выводит
     **/
    private static void printFile(ArrayList<Integer> arr) {

        for (int i = 0; i < arr.size(); i++) {

            //печатаем байты символов
            if (arr.get(i) < 0x10) {
                System.out.print("000");
                System.out.print(Integer.toHexString(+(arr.get(i)))+" ");
            } else if (arr.get(i) < 0x100) {
                System.out.print("00");
                System.out.print(Integer.toHexString(+(arr.get(i)))+" ");
            } else if (arr.get(i) < 0x1000) {
                System.out.print("0");
                System.out.print(Integer.toHexString(+(arr.get(i)))+" ");
            } else {
                System.out.print(Integer.toHexString(+(arr.get(i)))+" ");
            }
            //печатаем сами символы
            if ((i+1)%32 == 0 ) {
                if (arr.size() > i) {
                    for (int j = i-31; j < i; j++) {
                        System.out.printf("%s",(char)(int)arr.get(j));
                    }
                    System.out.println();
                } else {
                    System.out.println();
                }
            }
        }
    }

    public static class OneChannel {
        private String numb;
        private String name;
        private String typOfService;
        private String state;
        private String kodir;
        private String vpid;
        private String apid;
        private String datestr;
        private String comment;

        public OneChannel() {
        }

        public OneChannel (String numb, String name, String typOfService, String state, String kodir, String vpid, String apid, String datestr, String comment) {
            this.numb = numb;
            this.name = name;
            this.typOfService = typOfService;
            this.state = state;
            this.kodir = kodir;
            this.vpid = vpid;
            this.apid = apid;
            this.datestr = datestr;
            this.comment = comment;
        }

        public String getNumb() {
            return numb;
        }

        public void setNumb(String numb) {
            this.numb = numb;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTypOfService() {
            return typOfService;
        }

        public void setTypOfService(String typOfService) {
            this.typOfService = typOfService;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getKodir() {
            return kodir;
        }

        public void setKodir(String kodir) {
            this.kodir = kodir;
        }

        public String getVpid() {
            return vpid;
        }

        public void setVpid(String vpid) {
            this.vpid = vpid;
        }

        public String getApid() {
            return apid;
        }

        public void setApid(String apid) {
            this.apid = apid;
        }

        public String getDatestr() {
            return datestr;
        }

        public void setDatestr(String datestr) {
            this.datestr = datestr;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        @Override
        public String toString() {
            return "OneChannel{" +
                    "numb='" + numb + '\'' +
                    ", name='" + name + '\'' +
                    ", typOfService='" + typOfService + '\'' +
                    ", state='" + state + '\'' +
                    ", kodir='" + kodir + '\'' +
                    ", vpid='" + vpid + '\'' +
                    ", apid='" + apid + '\'' +
                    ", datestr='" + datestr + '\'' +
                    ", comment='" + comment + '\'' +
                    '}';
        }
    }
}
